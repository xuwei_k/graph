package graph

final case class NodeList(
  value: Map[String, Node]
) {

  def link(relation: (String, String)): NodeList = {
    val (a, b) = relation
    NodeList(
      value.updated(
        a, value(a).copy(neighbors = value(b) :: value(a).neighbors)
      ).updated(
        b, value(b).copy(neighbors = value(a) :: value(b).neighbors)
      )
    )
  }

  def moveAll: NodeList = {
    val dt = 0.1
    NodeList(
      value.map{ case (k, n) =>
        val f = n.neighbors.iterator.map(n.springForce).fold(Vec.zero)(_ + _)
        val x = value.values.collect{ case nn if n != nn =>
          n.replusiveForce(nn)
        }.fold(f)(_ + _)
        k -> n.moveEular(dt, x)
      }
    )
  }


}

