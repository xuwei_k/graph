package graph

import scala.util.Random

final case class Node(
  name: String,
  neighbors: List[Node],
  r: Vec,
  v: Vec
) {

  def springForce(n: Node): Vec = {
    val k = 0.1
    val l = 60.0
    val dx = r.x - n.r.x
    val dy = r.y - n.r.y
    val d2 = (dx * dx) + (dy * dy)
    if(d2 < 0.001){
      Vec(
        x = Random.nextDouble - 0.5,
        y = Random.nextDouble - 0.5
      )
    }else{
      val d = math.sqrt(d2)
      val cos = dx / d
      val sin = dy / d
      val dl = d - l
      Vec(
        x = -k * dl * cos,
        y = -k * dl * sin
      )
    }
  }

  def replusiveForce(n: Node): Vec = {
    val g = 500.0
    val dx = r.x - n.r.x
    val dy = r.y - n.r.y
    val d2 = (dx * dx) + (dy * dy)
    if(d2 < 0.001){
      Vec(
        x = Random.nextDouble - 0.5,
        y = Random.nextDouble - 0.5
      )
    }else{
      val d = math.sqrt(d2)
      val cos = dx / d
      val sin = dy / d
      Vec(
        x = g / d2 * cos,
        y = g / d2 * sin
      )
    }
  }

  def frictionalForce: Vec = {
    val m = 0.3
    Vec(
      x = -m * v.x,
      y = -m * v.y
    )
  }

  def moveEular(dt: Double, f: Vec): Node = {
    copy(
     r = Vec(
       x = r.x + (dt * v.x),
       y = r.y + (dt * v.y)
     ),
     v = Vec(
       x = v.x + (dt * f.x),
       y = v.y + (dt * f.y)
     )
    )
  }
}
