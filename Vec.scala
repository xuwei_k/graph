package graph

final case class Vec(x: Double, y: Double){
  def +(that: Vec): Vec =
    Vec(this.x + that.x, this.y + that.y)
}

object Vec {
  val zero = Vec(0, 0)
}


